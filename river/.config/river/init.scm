#!/usr/bin/env riverguile
!#

(use-modules (riverguile)
             (ice-9 match))

;; Simple helper macro to only spawn a program when no instace of it is
;; running already, so init can be re-run without all widgets being duplicated.
;; As you can see, we can still fall back to using shell tricks if we do not
;; wish to implement functionality in scheme.
 (define (stringify x)
   (cond ((string? x) x)
         ((number? x) (number->string x))
         ((symbol? x) (symbol->string x))))
(define-syntax spawn-once
  (syntax-rules ()
    ((spawn-once CMD ...)
     (let* ((lst (map stringify `(CMD ...)))
            (c (car lst))
            (cmd (string-join lst)))
       (R spawn (string-append "pgrep -A -f " c " > /dev/null || " cmd))))))


;;;;;;;;;;;;;;;;;;;;;;
;;                  ;;
;;    Appearance    ;;
;;                  ;;
;;;;;;;;;;;;;;;;;;;;;;
(R background-color 0x000000)
(R border-color-focused 0x036fc6)
(R border-color-unfocused 0x95c5ed)
(R border-width 2)

(R spawn "swaybg -m fill -i '/usr/share/backgrounds/archlinux/lone.jpg'")

;;;;;;;;;;;;;;;;;;
;;              ;;
;;    Inputs    ;;
;;              ;;
;;;;;;;;;;;;;;;;;;
;; TODO use globs for input config, once I am on river master
(R set-repeat 40 300)
(R keyboard-layout -options grp:toggle,ctrl:swapcaps us,mm)

;; (R:input *Touchpad natural-scroll enabled)
(R:input *Mouse
         (accel-profile flat)
         (pointer-accel 0.5))

;;;;;;;;;;;;;;;;;;;
;;               ;;
;;    Widgets    ;;
;;               ;;
;;;;;;;;;;;;;;;;;;;
(define lockcmd "swaylock -f -c 000000")
(define suspendcmd "systemctl suspend")

(spawn-once swayidle -w
              timeout 300 lockcmd
              timeout 600 suspendcmd
              before-sleep lockcmd)

(spawn-once waybar -c "~/.config/waybar/river.jsonc")
(spawn-once swaybg -m fill -i "/usr/share/backgrounds/archlinux/lone.jpg")
(spawn-once way-displays)
(spawn-once swaync)
(spawn-once udiskie --tray)
(spawn-once nm-applet --indicator)
(spawn-once "/opt/BreakTimer/breaktimer")
(spawn-once "/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1")


;;;;;;;;;;;;;;;;;;;
;;               ;;
;;    Pointer    ;;
;;               ;;
;;;;;;;;;;;;;;;;;;;
(R map-pointer normal Super BTN_LEFT move-view)
(R map-pointer normal Super BTN_RIGHT resize-view)
(R set-cursor-warp on-focus-change)
(R focus-follows-cursor always)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                        ;;
;;    Launchy Keybinds    ;;
;;                        ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(R:map normal
  ; common programs
  (Super Return spawn foot)
  (Super Z      spawn thunar)

  ; rofi menus
  (Super Esc spawn "rofi -show p -modi p:rofi-power-menu")
  (Super P   spawn "rofi -show combi")
  (Super R   spawn "rofi -show run")

  ; grim screen shot
  (Super         X spawn "grim -g \"$(slurp)\" $(xdg-user-dir PICTURES)/Screenshots/$(date +%Y-%m-%d_%H-%m-%s).png")
  (Super+Shift   X spawn "grim -g \"$(slurp)\" $(xdg-user-dir PICTURES)/Screenshots/$(date +%Y-%m-%d_%H-%m-%s).png")
  (Super+Control X spawn "grim -g \"$(slurp)\" -| wl-copy && notify-send \"Screenshot Clipped\""))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                         ;;
;;    Window Management    ;;
;;                         ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(R:map normal
  (Super+Shift C close)
  (Super F toggle-fullscreen)
  (Super+Shift Space toggle-float)

  (Super J focus-view next)
  (Super K focus-view previous)
  (Super+Shift J swap next)
  (Super+Shift K swap previous)

  (Super U zoom)

  (Super L focus-output next)
  (Super H focus-output previous)
  (Super+Shift L send-to-output next)
  (Super+Shift H send-to-output previous)

  ;; (Super Comma send-layout-cmd rivertile "main-ratio -0.05")
  ;; (Super Period send-layout-cmd rivertile "main-ratio +0.05")

  (Super+Alt H move left 100)
  (Super+Alt J move down 100)
  (Super+Alt K move up 100)
  (Super+Alt L move right 100)

  (Super+Alt+Control H snap left)
  (Super+Alt+Control J snap down)
  (Super+Alt+Control K snap up)
  (Super+Alt+Control L snap right)

  (Super+Alt+Shift H resize horizontal -100)
  (Super+Alt+Shift J resize vertical 100)
  (Super+Alt+Shift K resize vertical -100)
  (Super+Alt+Shift L resize horizontal 100))


;;;;;;;;;;;;;;;;
;;            ;;
;;    Tags    ;;
;;            ;;
;;;;;;;;;;;;;;;;
(for-each (lambda (i)
            (let* ((tagmask (ash 1 i))
                   ;; TODO check if river-bnf is installed.
                   (switch-cmd (string-append "river-bnf "
                                              (number->string tagmask))))
              (R:map normal
                (Super               ,(1+ i) spawn               ,switch-cmd)
                (Super+Shift         ,(1+ i) set-view-tags       ,tagmask)
                (Super+Contrtol      ,(1+ i) toggle-focused-tags ,tagmask)
                (Super+Shift+Control ,(1+ i) toggle-view-tags    ,tagmask))))
          (iota 9))

(let* ((tagmask (1- (ash 1 32)))
       (switch-cmd (string-append "river-bnf "
                                  (number->string tagmask))))
  (R:map normal
    (Super               0   spawn               ,switch-cmd)
    (Super+Shift         0   set-view-tags       ,tagmask)
    (Super+Contrtol      0   toggle-focused-tags ,tagmask)
    (Super+Shift+Control 0   toggle-view-tags    ,tagmask)
    (Super               Tab focus-previous-tags)
    (Super+Shift         Tab send-to-previous-tags)))


;;;;;;;;;;;;;;;;;;
;;              ;;
;;    Filter    ;;
;;              ;;
;;;;;;;;;;;;;;;;;;
(for-each (lambda (app-id) (R rule-add -app-id ,app-id float))
          '(mpv pavucontrol org.pipewire.Helvum wlroots))
(R rule-add -app-id thunderbird -title "Write*" float)
(R rule-add -app-id thunderbird -title "Write*" dimensions 800 800)


;;;;;;;;;;;;;;;;;;;;;;
;;                  ;;
;;    Media Keys    ;;
;;                  ;;
;;;;;;;;;;;;;;;;;;;;;;
(for-each (lambda (mode)
            (R:map ,mode
              (None XF86AudioRaiseVolume  spawn "wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%+")
              (None XF86AudioLowerVolume  spawn "wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%-")
              (None XF86AudioMute         spawn "wpctl set-mute @DEFAULT_SINK@ toggle")
              (None XF86AudioMicMute      spawn "pactl set-source-mute @DEFAULT_SOURCE@ toggle")

              (None XF86AudioMedia        spawn "playerctl play-pause")
              (None XF86AudioPlay         spawn "playerctl play-pause")
              (None XF86AudioPrev         spawn "playerctl previous")
              (None XF86AudioNext         spawn "playerctl next")

              (None XF86MonBrightnessUp   spawn "brightnessctl set +5%")
              (None XF86MonBrightnessDown spawn "brightnessctl set 5%-")))
          '("normal" "locked"))


;;;;;;;;;;;;;;;;;;
;;              ;;
;;    Layout    ;;
;;              ;;
;;;;;;;;;;;;;;;;;;
(R:map normal
  (Super+Control       H     send-layout-cmd riverguile "modify horizontal-split +0.1 0.1 0.9")
  (Super+Control       L     send-layout-cmd riverguile "modify horizontal-split -0.1 0.1 0.9")
  (Super+Control       K     send-layout-cmd riverguile "modify vertical-split -0.1 0.1 0.9")
  (Super+Control       J     send-layout-cmd riverguile "modify vertical-split +0.1 0.1 0.9")
  (Super+Control+Shift H     send-layout-cmd riverguile "modify horizontal-split +0.01 0.1 0.9")
  (Super+Control+Shift L     send-layout-cmd riverguile "modify horizontal-split -0.01 0.1 0.9")
  (Super+Control+Shift K     send-layout-cmd riverguile "modify vertical-split -0.01 0.1 0.9")
  (Super+Control+Shift J     send-layout-cmd riverguile "modify vertical-split +0.01 0.1 0.9")
  (Super+Control       Plus  send-layout-cmd riverguile "modify shrink-mod-w -0.05 0.45 1.0; modify shrink-mod-h -0.02 0.8 1.0")
  (Super+Control       Minus send-layout-cmd riverguile "modify shrink-mod-w +0.05 0.45 1.0; modify shrink-mod-h +0.02 0.8 1.0")
  (Super+Control       Space send-layout-cmd riverguile "cycle-layout"))

;; TODO should riverguile set this automatically when installing a layout-demand handler?
(R default-layout riverguile)

;; Layout parameters, estimated maximum size of average session allocated at launch.
(define opts (make-hash-table (* 10 6)))

(define (opts-key key tags output)
  "Turn generic key into output/tag-specific key to allow per output/tag values."
  (string->symbol (string-append (symbol->string key) "-"
                                 (number->string tags) "-"
                                 (number->string output))))

(define (get-opt key tags output)
   "Try to access key in opts. If not set, insert default."
   (let* ((actual-key (opts-key key tags output))
          (val (hash-ref opts actual-key)))
     (if val val
         (let ((default (match key
                          ('horizontal-split 2/3)
                          ('vertical-split 1/2)
                          ('stack-mod 0.95)
                          ('shrink-mod-w 1)
                          ('shrink-mod-h 1)
                          ('layout 0))))
           (hash-set! opts actual-key default)
           default))))

(define (layout:columns n x y w h tags output)
  "Arrange windows into columns, right to left."
  (let ((width (/ w n)))
    (map (lambda (i) (list (+ x (- w (* i width))) y width h))
         (iota n 1))))

(define (layout:rows n x y w h tags output)
  "Arrange windows into rows, top to bottom."
  (let ((height (/ h n)))
    (map (lambda (i) (list x (+ y (* (1- i) height)) w height))
         (iota n 1))))

(define (layout:stack n x y w h tags output)
  "Arrange windows akin to a stack of cards."
  (if (eq? n 1)
      (list (list x y w h))
      (let* ((mod (get-opt 'stack-mod tags output))
             (height (* mod h))
             (width (* mod w))
             (height-offset (/ (- h height) (- n 1)))
             (width-offset (/ (- w width) (- n 1))))
        (map (lambda (i) (list (+ x (* i width-offset))
                               (+ y (* i height-offset))
                               width height))
             (iota n)))))

(define layouts
  (list layout:columns layout:rows layout:stack)) 

(install-handler 'layout-demand
  (lambda (view-count width height tags output)
    ((list-ref layouts (get-opt 'layout tags output))
     view-count 0 0 width height tags output)))

(define (cycle-layout tags output)
  "Cycle through all available layouts."
  (let* ((actual-key (opts-key 'layout tags output))
         (current (get-opt 'layout tags output))
         (next (if (eq? current (1- (length layouts))) 0 (1+ current))))
     (hash-set! opts actual-key next)))

(define (modify key modifier lower upper tags output)
  "Modify a numerical value, identified by key, using modifier and clamp it to [lower, upper]."
  (let* ((actual-key (opts-key key tags output))
         (current (get-opt key tags output))
         (clamp (lambda (in l u)
                  (cond ((< in l) l)
                        ((> in u) u)
                        (else in))))
         (new (clamp (+ current modifier) lower upper)))
    (hash-set! opts actual-key new)))

(install-handler 'user-command
  (lambda (cmd tags output)
    (for-each (lambda (c)
                (let* ((cmd-lst (string-split (string-trim-both c) #\space))
                       (oper    (car cmd-lst)))
                  (match oper
                    ("cycle-layout" (cycle-layout tags output))
                    ("modify" (let ((key      (string->symbol (list-ref cmd-lst 1)))
                                    (modifier (string->number (list-ref cmd-lst 2)))
                                    (lower    (string->number (list-ref cmd-lst 3)))
                                    (upper    (string->number (list-ref cmd-lst 4))))
                                (modify key modifier lower upper tags output))))))
              (string-split cmd #\;))))


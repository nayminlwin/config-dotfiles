# Default config for sway
#
# Copy this to ~/.config/sway/config and edit it to your liking.
#
# Read `man 5 sway` for a complete reference.

### Variables
#
# Logo key. Use Mod1 for Alt.
set $mod Mod4
# Home row direction keys, like vim
set $left h
set $down j
set $up k
set $right l
# Your preferred terminal emulator
set $term foot
# Your preferred application launcher
# Note: pass the final command to swaymsg so that the resulting window can be opened
# on the original workspace that the command was run on.
# set $menu BEMENU_BACKEND=wayland j4-dmenu-desktop --dmenu='bemenu -i -p "▶" --tf "#bdffbd" --hb "#005885" --hf "#ffffff" --sf "#a3e0ff" --scf "#a3e0ff" --fn "Linux Biolinum" 12' --term='foot' | xargs swaymsg exec --

for_window [app_id="sway-launcher-desktop"] floating enable, border pixel 2, sticky enable
for_window [app_id="nmtui"] floating enable, border pixel 2, sticky enable
for_window [app_id="thunderbird" title="Write*"] floating enable, border pixel 2, sticky enable
for_window [app_id="com.viber.Viber" title="ViberPC"] floating enable, sticky enable, move position 75 ppt 5 ppt

set $menu exec rofi -show combi
set $menu_dri exec env DRI_PRIME=1 $term -c $HOME/.config/foot/foot-float.ini -a sway-launcher-desktop -e sway-launcher-desktop
set $suspend exec systemctl suspend

focus_on_window_activation none

# seat seat0 xcursor_theme "pArch-24"

# exec_always gsettings set org.gnome.desktop.interface cursor-theme "pArch-24"

default_border pixel 2
smart_borders on

### Output configuration
#
# Default wallpaper (more resolutions are available in /usr/share/backgrounds/sway/)
# output * bg ~/Pictures/wallpapers/plain-blue-paper-textured-background.jpg fill
#
# Example configuration:
#
#   output HDMI-A-1 resolution 1920x1080 position 1920,0
#
# You can get the names of your outputs by running: swaymsg -t get_outputs

### Idle configuration
#
# Example configuration:
#
# exec swayidle -w \
#          timeout 300 'swaylock -f -c 000000' \
#          timeout 600 'swaymsg "output * dpms off"' resume 'swaymsg "output * dpms on"' \
#          before-sleep 'swaylock -f -c 000000'
#
# This will lock your screen after 300 seconds of inactivity, then turn off
# your displays after another 300 seconds, and turn your screens back on when
# resumed. It will also lock your screen before your computer goes to sleep.

exec swaybg -m fill -i "/usr/share/backgrounds/archlinux/lone.jpg"

exec swayidle \
  timeout 300 'swaylock -f -c 000000' \
  timeout 600 'swaymsg "output * dpms off"' \
  resume 'swaymsg "output * dpms on"' \
  before-sleep 'swaylock -f -c 000000' &

# for_window [class="steam_app*"] inhibit_idle focus
# for_window [class="war3.exe"] fullscreen
for_window [class="^.*"] inhibit_idle fullscreen
for_window [app_id="^.*"] inhibit_idle fullscreen

# for_window [class="starcraft.exe*"] output HDMI-A-1 resolution 1152x864

# for_window [class="Loria.x86_64"] input "2522:9219:COMPANY_2.4G_Device_Mouse" map_to_output "HDMI-A-1"

exec swaync
exec udiskie --tray
exec nm-applet --indicator
exec /opt/BreakTimer/breaktimer
exec /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1
# exec_always {
#   pkill kanshi
#   kanshi &
# }

exec way-displays

### Input configuration
#
# Example configuration:
#
#   input "2:14:SynPS/2_Synaptics_TouchPad" {
#       dwt enabled
#       tap enabled
#       natural_scroll enabled
#       middle_emulation enabled
#   }

input type:touchpad {
    natural_scroll enabled
}
#
# You can get the names of your inputs by running: swaymsg -t get_inputs
# Read `man 5 sway-input` for more information about this section.

# Setting up monitors
# Laptop left, Monitor right


input "type:keyboard" {
  xkb_layout us,mm
  xkb_options "grp:toggle,ctrl:swapcaps"
  repeat_delay 300
  repeat_rate 50
}

input "type:pointer" {
  accel_profile flat
  pointer_accel 0.2
}

mouse_warping output

### Key bindings
#
# Basics:
#
    # Start a terminal
    bindsym $mod+Return exec $term

    # Power options
    bindsym $mod+Escape exec rofi -show p -modi p:rofi-power-menu

    # Kill focused window
    bindsym $mod+Shift+c kill

    # Start your launcher
    bindsym $mod+p exec $menu
    bindsym $mod+r exec "rofi -show run"
    bindsym $mod+Shift+p exec $menu_dri

    # Lock screen
    bindsym $mod+Shift+d exec "swaylock -f -c 000000"
    bindsym $mod+Shift+u $suspend

    # File Browser
    bindsym $mod+z exec "nemo"

    # Drag floating windows by holding down $mod and left mouse button.
    # Resize them with right mouse button + $mod.
    # Despite the name, also works for non-floating windows.
    # Change normal to inverse to use left mouse button for resizing and right
    # mouse button for dragging.
    floating_modifier $mod normal

    # Reload the configuration file
    bindsym $mod+q reload

    # Exit sway (logs you out of your Wayland session)
    bindsym $mod+Shift+q exec swaymsg exit

    # Print Screen
    bindsym $mod+x exec "grim -o $(swaymsg -t get_outputs | jq -r '.[] | select(.focused) | .name') $(xdg-user-dir PICTURES)/Screenshots/$(date +'%s.png')"
    bindsym $mod+Shift+x exec 'grim -g "$(slurp)" $(xdg-user-dir PICTURES)/Screenshots/$(date +'%s.png')'
#
# Moving around:
#
    # Move your focus around
    bindsym $mod+$left focus left
    bindsym $mod+$down focus down
    bindsym $mod+$up focus up
    bindsym $mod+$right focus right

    bindsym $mod+Tab focus next
    bindsym $mod+Shift+Tab focus prev

    bindsym $mod+Left focus left
    bindsym $mod+Down focus down
    bindsym $mod+Up focus up
    bindsym $mod+Right focus right

    bindsym $mod+Control+$left focus output left
    bindsym $mod+Control+$right focus output right
    bindsym $mod+Control+Left focus output left
    bindsym $mod+Control+Right focus output right

    # Move the focused window with the same, but add Shift
    bindsym $mod+Shift+$left move left
    bindsym $mod+Shift+$down move down
    bindsym $mod+Shift+$up move up
    bindsym $mod+Shift+$right move right

    # Ditto, with arrow keys
    bindsym $mod+Shift+Left move left
    bindsym $mod+Shift+Down move down
    bindsym $mod+Shift+Up move up
    bindsym $mod+Shift+Right move right

    bindsym $mod+Control+Shift+$left move output left
    bindsym $mod+Control+Shift+$right move output right
    bindsym $mod+Control+Shift+Left move output left
    bindsym $mod+Control+Shift+Right move output right

#
# Workspaces:
#
    # Switch to workspace
    bindsym $mod+1 workspace number 1
    bindsym $mod+2 workspace number 2
    bindsym $mod+3 workspace number 3
    bindsym $mod+4 workspace number 4
    bindsym $mod+5 workspace number 5
    bindsym $mod+6 workspace number 6
    bindsym $mod+7 workspace number 7
    bindsym $mod+8 workspace number 8
    bindsym $mod+9 workspace number 9
    bindsym $mod+0 workspace number 10
    # Move focused container to workspace
    bindsym $mod+Shift+1 move container to workspace number 1
    bindsym $mod+Shift+2 move container to workspace number 2
    bindsym $mod+Shift+3 move container to workspace number 3
    bindsym $mod+Shift+4 move container to workspace number 4
    bindsym $mod+Shift+5 move container to workspace number 5
    bindsym $mod+Shift+6 move container to workspace number 6
    bindsym $mod+Shift+7 move container to workspace number 7
    bindsym $mod+Shift+8 move container to workspace number 8
    bindsym $mod+Shift+9 move container to workspace number 9
    bindsym $mod+Shift+0 move container to workspace number 10
    # Note: workspaces can have any name you want, not just numbers.
    # We just use 1-10 as the default.

    # Move workspaces between outputs
    bindsym $mod+Shift+e move workspace to output right
    bindsym $mod+Shift+w move workspace to output left
    bindsym $mod+Shift+s exec ~/scripts/swap_workspaces.sh

#
# Layout stuff:
#
    # You can "split" the current object of your focus with
    # $mod+b or $mod+v, for horizontal and vertical splits
    # respectively.
    bindsym $mod+b splith
    bindsym $mod+v splitv
    bindsym $mod+n split none

    # Switch the current container between different layout styles
    # bindsym $mod+s layout stacking
    bindsym $mod+w layout tabbed
    bindsym $mod+e layout toggle split

    # Make the current focus fullscreen
    bindsym $mod+f fullscreen

    # Toggle the current focus between tiling and floating mode
    bindsym $mod+Shift+space floating toggle

    # Swap focus between the tiling area and the floating area
    bindsym $mod+space focus mode_toggle

    # Move focus to/from parent container
    bindsym $mod+u focus parent
    bindsym $mod+d focus child
#
# Scratchpad:
#
    # Sway has a "scratchpad", which is a bag of holding for windows.
    # You can send windows there and get them back later.

    # Move the currently focused window to the scratchpad
    bindsym $mod+Shift+minus move scratchpad

    # Show the next scratchpad window or hide the focused scratchpad window.
    # If there are multiple scratchpad windows, this command cycles through them.
    bindsym $mod+minus scratchpad show
#
# Resizing containers:
#
mode "resize" {
    # left will shrink the containers width
    # right will grow the containers width
    # up will shrink the containers height
    # down will grow the containers height
    bindsym $left resize shrink width 20px
    bindsym $down resize grow height 20px
    bindsym $up resize shrink height 20px
    bindsym $right resize grow width 20px

    # Ditto, with arrow keys
    bindsym Left resize shrink width 20px
    bindsym Down resize grow height 20px
    bindsym Up resize shrink height 20px
    bindsym Right resize grow width 20px

    # Return to default mode
    bindsym Return mode "default"
    bindsym Escape mode "default"
}
bindsym $mod+Shift+r mode "resize"

# Function Key Bindings
#
bindsym XF86AudioRaiseVolume exec wpctl set-volume @DEFAULT_SINK@ 5%+
bindsym XF86AudioLowerVolume exec wpctl set-volume @DEFAULT_SINK@ 5%-
bindsym XF86AudioMute exec wpctl set-mute @DEFAULT_SINK@ toggle
bindsym XF86AudioMicMute exec pactl set-source-mute @DEFAULT_SOURCE@ toggle
bindsym XF86MonBrightnessDown exec brightnessctl set 5%-
bindsym XF86MonBrightnessUp exec brightnessctl set +5%
bindsym XF86AudioPlay exec playerctl play-pause
bindsym XF86AudioNext exec playerctl next
bindsym XF86AudioPrev exec playerctl previous

# Color classes
client.focused          #576F74 #2E474D #ffffff
client.focused_inactive #5f676a #373D3F #ffffff
client.unfocused        #555555 #303030 #ffffff
client.urgent           #A97878 #674949 #ffffff
client.placeholder      #000000 #0c0c0c #ffffff

#
# Status Bar:
#
# Read `man 5 sway-bar` for more information about this section.
bar {
  swaybar_command waybar
  # position bottom

  # status_command while date +'%Y-%m-%d %I:%M:%S %P'; do sleep 1; done

  # colors {
  #   statusline #ffffff
  #   background #192330
  #   active_workspace #B7B7A4 #212e3f #ffffff
  #   inactive_workspace #B7B7A4 #29394f #ffffff
  # }

  # icon_theme Adwaita

}

include /etc/sway/config.d/*

